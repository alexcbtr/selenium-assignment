package ca.blinov.selenium.tests;

import ca.blinov.selenium.pages.*;
import com.lazerycode.selenium.DriverBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.WebDriver;

public class CreditCardPurchaseIT extends DriverBase {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() throws Exception {
        driver = getDriver();
    }

    @Test
    public void verifyErrorBuyingWithInvalidCreditCard() {
        // Open home page
        HomePage homePage = new HomePage(driver);
        homePage.open();

        // Search for product
        homePage.setSearchInput("MacBook Pro 13\"");
        SearchResultsPage searchResultsPage = homePage.pressEnterInSearch();

        // Click on product in the list
        Assert.assertTrue(searchResultsPage.getNumberOfDisplayedProducts() > 0,
                "Expected at least one product to be displayed on search results page. None were displayed");
        String productTitle = "Apple MacBook Pro 13.3\" Laptop (Intel Core i5 2.3GHz / 128GB SSD / 8GB RAM) - Space Grey - English";
        Assert.assertTrue(searchResultsPage.getNumberOfProductLinksByPartialText(productTitle) > 0,
                String.format("Expected at least one product with Link Text '%s' to be displayed. Found none.", productTitle));

        ProductDetailsPage productDetailsPage = searchResultsPage.clickOnFirstProductLinkByPartialText(productTitle);

        // Add product to Shopping Cart
        Assert.assertEquals(productTitle, productDetailsPage.getProductTitle(),
                "Expected to be on Product Details Page.");
        AddToCartConfirmationPage addToCartConfirmationPage = productDetailsPage.clickAddToCartButton();

        // Confirmation is displayed. Add Warranty plan.
        Assert.assertTrue(addToCartConfirmationPage.isAcceptTermsCheckboxVisible(),
                "Expected to see Accept Terms checkbox in Add Warranty modal.");
        Assert.assertTrue(addToCartConfirmationPage.isAddWarrantyButtonVisible(),
                "Expected to see Add Warranty button on Add Warranty modal." );
        addToCartConfirmationPage.clickAcceptTermsCheckbox();
        ShoppingCartPage shoppingCartPage = addToCartConfirmationPage.clickAddWarrantyButton();

        // Shopping cart is displayed. Checkout now
        Assert.assertTrue(shoppingCartPage.isCheckoutButtonDisplayed(),
                "Expected Checkout Button to be displayed on shopping cart page");
        CheckoutTypePage checkoutTypePage = shoppingCartPage.clickCheckoutButton();
        Assert.assertTrue(checkoutTypePage.isContinueCheckoutButtonDisplayed(),
                "Expected Continue button to be displayed on Sign-in or Checkout As Guest page");

        // Sign in or guest checkout is requested - Checkout as Guest
        CheckoutDeliveryPage checkoutDeliveryPage = checkoutTypePage.clickContinueCheckoutButton();

        // Fill Delivery Form
        Assert.assertTrue(checkoutDeliveryPage.isDeliveryAddressFormDisplayed(),
                "Expected to see New Delivery Address form on Checkout Page");
        checkoutDeliveryPage.fillForm(
                "Test",
                "User",
                "8800 Glenlyon Parkway",
                "Burnaby",
                "British Columbia",
                "Canada",
                "V5J5K3",
                "866",
                "237",
                "8289");
        Assert.assertTrue(checkoutDeliveryPage.isContinueButtonDisplayed(),
                "Expected to see Continue Button on Checkout Page");
        CheckoutPaymentPage checkoutPaymentPage = checkoutDeliveryPage.clickContinueButton();
        Assert.assertFalse(checkoutDeliveryPage.isErrorDisplayed(),
                "Expected no errors to be displayed after filling Delivery Address form.");

        // Fill Credit Card form
        String nextYear = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) + 1);

        Assert.assertTrue(checkoutPaymentPage.waitForSelectButtonToBeVisible(),
                "Expected Select Credit Card button to become visible in reasonable time after Address Form submission. That did not happen.");
        checkoutPaymentPage.clickSelectPaymentMethodCreditCardButton();
        Assert.assertTrue(checkoutPaymentPage.isCreditCardFormVisible(), "Expected Credit Card Form to be visible after clicking Select Credit Card Button.");
        checkoutPaymentPage.fillCreditCardData(
                "Visa",
                "4637903132144964",
                "12",
                nextYear, // using next year instead of exact date, to avoid extra error which could start apprearing in future
                "" // There is an issue with CVV field in product, which I tried to workaround but needed more time
        );
        Assert.assertTrue(checkoutPaymentPage.isBillingAddressSameAsShippingCheckboxVisible(),
                "Expected Billing Address is the same as Shipping Address checkbox to be visible on Checkout Page");
        checkoutPaymentPage.clickBillingAddressSameAsShippingCheckbox();
        // checkoutPaymentPage.fillConfirmationEmail("test@example.com"); //XXX
        Assert.assertTrue(checkoutPaymentPage.isContinueButtonVisible(),
                "Expected Continue Button to be visible on Payment section of Checkout Page");
        checkoutPaymentPage.clickContinueButton();

        List<String> expectedErrorMessageList = Arrays.asList(
                "The format of the credit card number entered does not match the card type you specified. Please check for accuracy and try again. (0018)",
                "Please enter the CID for your credit card. (0096)"
                );
        Assert.assertTrue(checkoutPaymentPage.isCardNumberErrorImageDisplayed(),
                "Expected to see error image next to Credit Card number filed");
        Assert.assertEquals(expectedErrorMessageList.get(0), checkoutPaymentPage.getCardNumberErrorImageAlt());
        Assert.assertTrue(checkoutPaymentPage.isCardCVVErrorImageVisibie(),
                "Expected to see error image next to CVV filed");
        Assert.assertEquals(expectedErrorMessageList.get(1), checkoutPaymentPage.getCardCVVErrorImageAlt());
        Assert.assertTrue(checkoutPaymentPage.isErrorDisplayed(),
                "Expected Error to be displayed on Checkout Page after submission of invalid Credit Card form");
        Assert.assertEquals(expectedErrorMessageList, checkoutPaymentPage.getErrorList());
    }
}
