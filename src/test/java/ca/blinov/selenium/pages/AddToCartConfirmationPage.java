package ca.blinov.selenium.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddToCartConfirmationPage extends BasePage {

    public AddToCartConfirmationPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(className = "cart-message")
    private WebElement confirmationMessage;

    @FindBy(className = "warranty-add-trigger")
    private WebElement addWarrantyButton;

    @FindBy(id = "accept-terms")
    private WebElement acceptTermsCheckbox;

    @Step("On Add to cart confirmation page, click Add Warranty button")
    public ShoppingCartPage clickAddWarrantyButton() {
        addWarrantyButton.click();
        return new ShoppingCartPage(driver);
    }

    public boolean isAcceptTermsCheckboxVisible(){
        return isElementVisible(acceptTermsCheckbox);
    }

    public boolean isAddWarrantyButtonVisible(){
        return isElementVisible(addWarrantyButton);
    }

    @Step("On Add to cart confirmation page, check Accept Terms and Conditions checkbox")
    public void clickAcceptTermsCheckbox(){
        acceptTermsCheckbox.click();
    }
}
