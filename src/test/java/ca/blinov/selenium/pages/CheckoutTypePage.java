package ca.blinov.selenium.pages;


import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckoutTypePage extends BasePage{

    public CheckoutTypePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ctl00_CP_UcCheckoutSignInUC_btnSubmitOrder")
    private WebElement continueCheckoutButton;

    public boolean isContinueCheckoutButtonDisplayed(){
        return isElementVisible(continueCheckoutButton);
    }

    @Step("On Checkout Type page click Continue button")
    public CheckoutDeliveryPage clickContinueCheckoutButton() {
        continueCheckoutButton.click();
        return new CheckoutDeliveryPage(driver);
    }
}
