package ca.blinov.selenium.pages;


import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CheckoutDeliveryPage extends BasePage{

    public CheckoutDeliveryPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_pnlNewShippingAddress")
    private WebElement deliveryAddressForm;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_FirstNameContainer_TxtFirstName")
    private WebElement firstNameInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_LastNameContainer_txtLastName")
    private WebElement lastNameInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_AddressLine1Container_TxtAddressLine1")
    private WebElement addressInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_CityContainer_TxtCity")
    private WebElement cityInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_StateContainer_DdlState")
    private WebElement provinceDropdown;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PostalCodeContainer_TxtZipCode")
    private WebElement postalCodeInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_CountryContainer_DdlCountry")
    private WebElement countryDropdown;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneContainer_TxtPhone")
    private WebElement phoneAreaCodeInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_Phone1Container_TxtPhone1")
    private WebElement phoneNxxInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_Phone2Container_TxtPhone2")
    private WebElement phoneNumberInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_btnContinueFromShipping")
    private WebElement continueButton;

    @FindBy(id = "ctl00_CP_ErrorSummaryUC2_ValidationSummary1")
    private WebElement validationErrorsBlock;

    @Step("On Checkout Page Fill Delivery Step Form")
    public void fillForm(String firstName,
                         String lastName,
                         String address,
                         String city,
                         String province,
                         String country,
                         String postalCode,
                         String phoneAreaCode,
                         String phoneNxx,
                         String phoneNumber){
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setCity(city);
        setProvince(province);
        setCountry(country);
        setPostalCode(postalCode);
        setPhone(phoneAreaCode, phoneNxx, phoneNumber);
    }

    public boolean isDeliveryAddressFormDisplayed(){
        return isElementVisible(deliveryAddressForm);
    }

    public boolean isContinueButtonDisplayed(){
        return isElementVisible(continueButton);
    }

    public void setFirstName(String firstName){
        firstNameInput.sendKeys(firstName);
    }

    public void setLastName(String lastName){
        lastNameInput.sendKeys(lastName);
    }

    public void setAddress(String address){
        addressInput.sendKeys(address);
    }

    public void setCity(String city){
        cityInput.sendKeys(city);
    }

    public void setProvince(String province){
        Select select = new Select(provinceDropdown);
        select.selectByVisibleText(province);
    }

    public void setCountry(String country){
        Select select = new Select(countryDropdown);
        select.selectByVisibleText(country);
    }

    public void setPostalCode(String postalCode){
        postalCodeInput.clear();
        postalCodeInput.sendKeys(postalCode);
    }

    public void setPhone(String areaCode, String nxx, String number){
        phoneAreaCodeInput.sendKeys(areaCode);
        phoneNxxInput.sendKeys(nxx);
        phoneNumberInput.sendKeys(number);
    }

    public boolean isErrorDisplayed(){
        return isElementVisible(validationErrorsBlock);
    }

    @Step
    public CheckoutPaymentPage clickContinueButton() {
        continueButton.click();
        return new CheckoutPaymentPage(driver);
    }
}
