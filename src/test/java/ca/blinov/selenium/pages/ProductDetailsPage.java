package ca.blinov.selenium.pages;


import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductDetailsPage extends BasePage {

    public ProductDetailsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ctl00_CP_ctl00_PD_lblProductTitle")
    private WebElement productTitle;

    @FindBy(id = "btn-cart")
    private WebElement addToCartButton;

    public String getProductTitle() {
        return productTitle.getText();
    }

    @Step("Click add to cart button")
    public AddToCartConfirmationPage clickAddToCartButton() {
        addToCartButton.click();
        return new AddToCartConfirmationPage(driver);
    }
}
