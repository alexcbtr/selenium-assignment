package ca.blinov.selenium.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    private String url = "http://bestbuy.ca";

    @FindBy(id = "ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword")
    private WebElement searchInput;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Step("Open site home page")
    public void open(){
        driver.get(url);
    }

    @Step("Search for product: {searchValue}")
    public void setSearchInput(String searchValue) {
        searchInput.sendKeys(searchValue);
    }

    public SearchResultsPage pressEnterInSearch() {
        searchInput.sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver);
    }


}
