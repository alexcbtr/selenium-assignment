package ca.blinov.selenium.pages;


import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ShoppingCartPage extends BasePage{

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(partialLinkText = "Checkout")
    private WebElement checkoutButton;

    @FindBy(className = "cart-message")
    private WebElement cartMessage;

    public boolean isCheckoutButtonDisplayed(){
        return isElementVisible(checkoutButton);
    }

    @Step("On Shopping Cart page, Click Checkout button")
    public CheckoutTypePage clickCheckoutButton() {
        checkoutButton.click();
        return new CheckoutTypePage(driver);
    }
}
