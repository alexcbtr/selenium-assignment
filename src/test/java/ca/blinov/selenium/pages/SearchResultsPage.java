package ca.blinov.selenium.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(className = "listing-item")
    private List <WebElement> productList;

    public int getNumberOfDisplayedProducts(){
        return productList.size();
    }

    public List <WebElement> getProductLinkListByText(String text) {
        return driver.findElements(By.partialLinkText(text));
    }

    public int getNumberOfProductLinksByPartialText(String text){
        return getProductLinkListByText(text).size();
    }

    @Step("Click on product link by partial text: {text}")
    public ProductDetailsPage clickOnFirstProductLinkByPartialText(String text) {
        clickOnFirstElementInList(getProductLinkListByText(text));
        return new ProductDetailsPage(driver);
    }
}
