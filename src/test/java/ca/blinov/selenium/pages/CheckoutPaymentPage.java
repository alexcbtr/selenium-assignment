package ca.blinov.selenium.pages;


import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;

public class CheckoutPaymentPage  extends BasePage{

    public CheckoutPaymentPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_lnkCreditCard")
    private WebElement selectPaymentMethodCreditCardButton;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CardTypeContainer_DdlCardType")
    private WebElement cardTypeInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CreditCardNumberContainer_TxtCardNumber")
    private WebElement cardNumberInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_MonthContainer_DdlMonth")
    private WebElement cardExpirationMonthDropdown;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_YearContainer_DdlYear")
    private WebElement cardExpirationYearDropdown;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_CIDNumberContainer_TxtCID")
    private WebElement cardCVVInput;

    @FindBy(id = "CreditCardEditTxtCid")
    private WebElement CVVEditHiddenDiv;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_ChkSameAsShipping")
    private WebElement billingAddressSameAsShippingCheckbox;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_TxtEmailAddress")
    private WebElement confirmationEmailInput;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_BtnContinueFromPayment")
    private WebElement continueButton;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CreditCardNumberContainer_ctl00_imgCardNumber")
    private WebElement cardNumberErrorImage;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CIDNumberContainer_ctl00_imgCID")
    private WebElement cardCVVErrorImage;

    @FindBy(id = "ctl00_CP_ErrorSummaryUC12_ValidationSummary1")
    private WebElement validationErrorsBlock;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_PnlEnterCreditCard")
    private WebElement creditCardForm;

    public void setCardType(String cardType){
        Select select = new Select(cardTypeInput);
        select.selectByVisibleText(cardType);
    }

    public void setCardNumber(String cardNumber){
        cardNumberInput.sendKeys(cardNumber);
    }

    public void setCardExpirationMonth(String cardExpirationMonth){
        Select select = new Select(cardExpirationMonthDropdown);
        select.selectByVisibleText(cardExpirationMonth);
    }

    public void setCardExpirationYear(String cardExpirationYear){
        Select select = new Select(cardExpirationYearDropdown);
        select.selectByVisibleText(cardExpirationYear);
    }

    // TODO: CVVEditHiddenDiv has 0 height in product, which makes cardCVVInput not visible
    // Which in tern triggers ElementNotVisible exception in WebDriver
    // Correct way to deal with this situation is to submit a fix for product, as this is potential cross browser problem
    // Possible workaround is to change div height via javascript to make it visible, but it needs more work
    public void setCardCVV(String cvv){
        System.out.println("Parent div of CVV row is displayed: " + CVVEditHiddenDiv.isDisplayed());
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.height='29px'", CVVEditHiddenDiv);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(cardCVVInput));
        cardCVVInput.sendKeys(cvv);
    }

    public boolean waitForSelectButtonToBeVisible(){
        return waitForElementVisibility(selectPaymentMethodCreditCardButton, 10);
    }

    public boolean isCreditCardFormVisible(){
        return isElementVisible(creditCardForm);
    }

    public boolean isBillingAddressSameAsShippingCheckboxVisible(){
        return isElementVisible(billingAddressSameAsShippingCheckbox);
    }

    public boolean isContinueButtonVisible(){
        return isElementVisible(continueButton);
    }

    public List<String> getErrorList(){
        List<WebElement> errorElementList = validationErrorsBlock.findElements(By.tagName("li"));
        return errorElementList.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }


    public void clickSelectPaymentMethodCreditCardButton(){
        selectPaymentMethodCreditCardButton.click();
    }

    public void fillConfirmationEmail(String email){
        confirmationEmailInput.sendKeys(email);
    }

    @Step("On Checkout Page, Fill Credit Card Data")
    public void fillCreditCardData(
            String cardType,
            String cardNumber,
            String cardExpirationMonth,
            String cardExpirationYear,
            String cardCVV){
        setCardType(cardType);
        setCardNumber(cardNumber);
        setCardExpirationMonth(cardExpirationMonth);
        setCardExpirationYear(cardExpirationYear);
        // setCardCVV(cardCVV); TODO: I have tried to workaround product problem, but needed more time, see above
    }

    @Step("Click Billing Address is the same as Shipping Address checkbox")
    public void clickBillingAddressSameAsShippingCheckbox(){
        billingAddressSameAsShippingCheckbox.click();
    }

    @Step("Click Continue Button")
    public void clickContinueButton(){
        continueButton.click();
    }

    public boolean isCardNumberErrorImageDisplayed(){
        return isElementVisible(cardNumberErrorImage);
    }

    public String getCardNumberErrorImageAlt(){
        return cardNumberErrorImage.getAttribute("alt");
    }

    public boolean isCardCVVErrorImageVisibie() {
        return isElementVisible(cardCVVErrorImage);
    }

    public String getCardCVVErrorImageAlt() {
        return cardCVVErrorImage.getAttribute("alt");
    }

    public boolean isErrorDisplayed(){
        return isElementVisible(validationErrorsBlock);
    }

}
