Selenium Assignment
=======================

### Prerequisites
Following software is required to run the assignment tests
- Java 1.8
- Maven
- Chrome

### How to execute

1. Open a terminal window
1. Clone this project.
1. `cd selenium-assignment` (Or whatever folder you cloned it into)
1. `mvn clean verify`
1. `mvn allure:serve` to view the report

All dependencies will be downloaded, one test which verifies checkout with invalid credit card is executed


### Can I use another browser?

Yes you can specify which browser to use by using one of the following switches:

- -Dbrowser=firefox
- -Dbrowser=chrome
- -Dbrowser=ie
- -Dbrowser=edge
- -Dbrowser=opera
- -Dbrowser=htmlunit
- -Dbrowser=phantomjs

### Known Limitations

- Potential failure due to survey modal, which might be randomly displayed on various pages.
- CVV field is not being filled on Credit Card form. More information in CheckoutPaymentPage.class

### If something is not working

You have probably got outdated driver binaries, by default they are not overwritten if they already exist to speed things up.  You have two options:

- `mvn clean verify -Doverwrite.binaries=true`
- Delete the `selenium_standalone_binaries` folder in your resources directory


### Tools used
- [Selenium Maven Template](https://github.com/Ardesco/Selenium-Maven-Template) for WebDriver binaries management and initial maven configuration
- [Allure](https://github.com/allure-framework/allure2) for reporting
